//
//  SceneDelegate.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import UIKit
import BackgroundTasks
import UserNotifications


class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    let notifyManager = NotificationManager()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = windowScene
        guard window != nil else { return }
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.pavelbelov.fetchRates", using: nil, launchHandler: { task in
            self.handleBackgroundTask(task: task as! BGAppRefreshTask)
        })
        scheduleAppRefresh()
        let coordinator = Coordinator(window: window!)
        coordinator.start()
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        scheduleAppRefresh()
    }
    
    
    let operation = BlockOperation {
        let bgtasker = BGHandler()
        bgtasker.configure()
    }


}

extension SceneDelegate {
     
    func scheduleAppRefresh() {
        do {
            let request = BGAppRefreshTaskRequest(identifier: "com.pavelbelov.fetchRates")
            request.earliestBeginDate = nil//Date(timeIntervalSinceNow: 10)
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("ERR \(error.localizedDescription)")
        }
    }
    
    func handleBackgroundTask(task: BGAppRefreshTask) {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.addOperation(self.operation)
        task.expirationHandler = {
            queue.cancelAllOperations()
        }
        let lastOperation = queue.operations.last
        lastOperation?.completionBlock = {
            task.setTaskCompleted(success: true)
        }
        scheduleAppRefresh()
    }
    
}

//e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"com.pavelbelov.fetchRates"]
