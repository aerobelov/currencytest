//
//  AddAlertViewModel.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class AddAlertViewModel {
    
    var storage = StorageService()
    
    func saveAlert(for value: Double) {
        storage.saveAlert(for: value)
    }
    
    func getAlert() -> String? {
        return storage.get()
    }
}
