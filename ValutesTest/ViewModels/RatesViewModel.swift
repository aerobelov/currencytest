//
//  RatesViewModel.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class RatesViewModel: NSObject, XMLParserDelegate {
    
    var parser: XMLParser!
    var items: [Record] = []
    var onUpdate: (() -> Void)?
    var currentRecord: Record?
    var currentNominal: String?
    var currentValue: String?
    var currentDate: String?
    var found: String = ""

    func fetch() {
        let manager = SessionManager(type: .month)
        manager.fetchData(completion: { [weak self] data in
            self?.parseXML(data: data)
        })
    }
    
    func parseXML(data: Data?) {
        guard data != nil else { return }
        parser = XMLParser(data: data!)
        parser.delegate = self
        parser.parse()
        self.onUpdate?()
        
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "Record" {
            currentDate=""
            if let date = attributeDict["Date"] {
                self.currentDate = date
            }
            self.currentRecord = Record()
        } else if elementName == "Nominal" {
            self.currentNominal = ""
        } else if elementName == "Value" {
            self.currentValue = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        self.found += string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Record" {
            currentRecord!.date = currentDate
            self.items.append(self.currentRecord!)
        } else if elementName == "Nominal" {
            self.currentRecord?.nominal = self.found
        } else if elementName == "Value" {
            self.currentRecord?.value = self.found
        }
        self.found = ""
    }
    
}
