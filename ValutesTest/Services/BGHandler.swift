//
//  BGHandler.swift
//  ValutesTest
//
//  Created by Pavel Belov on 05.11.2021.
//

import Foundation
import UIKit

class BGHandler {
    var service = BackgroundService()
    var sessionManager = SessionManager(type: .latest)
    let notManager = NotificationManager()
    
    init() {
        service.onUpdate = {
            self.update()
        }
    }
     
    func configure() {
        print("CONFIGURE")
        sessionManager.fetchData  { data in
            self.service.parseXML(data: data)
        }
    }
    
    func update() {
        if let val = self.service.latestValue {
            self.service.latestValue = val
            let storage = StorageService()
            if let old = storage.get() {
                print("INSIDE SEND NOTIFY")
                print(old, val)
                if val>old {
                    notManager.sendNotification(value: val)
                }
            }
        }
    }
    
}
