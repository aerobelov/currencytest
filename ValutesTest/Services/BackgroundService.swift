//
//  BackgroundService.swift
//  ValutesTest
//
//  Created by Pavel Belov on 05.11.2021.
//

import Foundation

class BackgroundService: NSObject, XMLParserDelegate {
    
    var parser: XMLParser!
    var latestValue: String?
    var currentValute: Valute?
    var currentID: String?
    var currentValue: String?
    var found: String = ""
    var onUpdate: (() -> Void)?
    
    
    
    func parseXML(data: Data?) {
        guard data != nil else { return }
        parser = XMLParser(data: data!)
        parser.delegate = self
        parser.parse()
        self.onUpdate?()
    }
    
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "Valute" {
            if let id = attributeDict["ID"] {
                self.currentID = id
            }
        }  else if elementName == "Value" {
            self.currentValue = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        self.found += string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Valute" {
            if currentID == "R01235" {
                latestValue = currentValue
            }
            currentID = ""
            
        } else if elementName == "Value" {
            currentValue = self.found
        }
        self.found = ""
    }
    
    
}
