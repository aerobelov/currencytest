//
//  DatesBuilder.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class DatesBuilder {
    
    var fromDate: String?
    var toDate: String?
    
    init() {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let past = Calendar.current.date(byAdding: Calendar.Component.month, value: -1, to: today)
        guard past != nil else {return }
        self.fromDate = formatter.string(from: past!)
        self.toDate = formatter.string(from: today)
    }
}
