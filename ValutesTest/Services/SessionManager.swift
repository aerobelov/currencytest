//
//  SessionManager.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class SessionManager {
    
    var defaultSession = URLSession.init(configuration: .default)
    var type: SessionType!
    let datesBuilder = DatesBuilder()
    var urlBuilder: URLBuilder?
    
    init(type: SessionType) {
        self.type = type
    }
    
    func fetchData(completion: @escaping ((Data)->Void?)) {
        
        switch self.type {
        case .month:
            guard datesBuilder.fromDate != nil, datesBuilder.toDate != nil else { return }
            self.urlBuilder = URLBuilder(fromDate: datesBuilder.fromDate!, toDate: datesBuilder.toDate!)
        case .latest:
            self.urlBuilder = URLBuilder()
        default:
            self.urlBuilder = URLBuilder()
        }
        if let url = self.urlBuilder?.url {
            print(url)
            defaultSession.dataTask(with: url, completionHandler: { data, response, error in
                guard data != nil, error == nil else {return}
                completion(data!)
            })
            .resume()
        }
    }
}


enum SessionType {
    case month, latest
}
