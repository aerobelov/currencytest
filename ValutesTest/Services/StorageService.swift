//
//  StorageService.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class StorageService {
    
    var defaults = UserDefaults.standard
    
    func saveAlert(for value: Double) {
        defaults.set(value, forKey: "alertValue")
        defaults.set(true, forKey: "isSubscribed")
    }
    
    func get() -> String? {
        let val = defaults.double(forKey: "alertValue")
        return String(val)
    }
    
}
