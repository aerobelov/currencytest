//
//  URLBuilder.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class URLBuilder {
    
    var url: URL?
    
    init(fromDate: String, toDate: String) {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "cbr.ru"
        components.path = "/scripts/XML_dynamic.asp"
        let fromQuery = URLQueryItem(name: "date_req1", value: fromDate)
        let toQuery = URLQueryItem(name: "date_req2", value: toDate)
        let currencyQuery = URLQueryItem(name: "VAL_NM_RQ", value: "R01235")
        components.queryItems = [fromQuery, toQuery, currencyQuery]
        if let url = components.url {
            self.url = url
        }
    }
    
    init() {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "cbr.ru"
        components.path = "/scripts/XML_daily.asp"
        if let url = components.url {
            self.url = url
        }
    }

}
