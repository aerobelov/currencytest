//
//  Extensions.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

extension Double {
    func stringValue() -> String? {
        return String(self)
    }
}

extension String {
    func doubleValue() -> Double? {
        return Double(self)
    }
}
