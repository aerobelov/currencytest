//
//  NotificationsManager.swift
//  ValutesTest
//
//  Created by Pavel Belov on 05.11.2021.
//

import Foundation
import UserNotifications

class NotificationManager: NSObject {
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    override init() {
        super.init()
        notificationCenter.delegate = self
    }
    
    func request()  {
        self.notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            
        }
    }
    
    func sendNotification(value: String) {
        
        self.notificationCenter.getNotificationSettings { [self] settings in
            guard settings.authorizationStatus == .authorized else { return }
            let content = UNMutableNotificationContent()
            content.title = "Курс валют изменился"
            content.body = "Курс USD \(value)"
            content.sound = UNNotificationSound.default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest(identifier: "notification", content: content, trigger: trigger)
            self.notificationCenter.add(request) { error in
                if let error = error {
                    print("Notify ERROR \(error.localizedDescription)")
                }
                
            }
        }
    }
    
    
}

extension NotificationManager: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .sound])
        print(#function)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(#function)
    }
}
