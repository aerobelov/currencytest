//
//  Record.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

class Record {
    var date: String?
    var nominal: String?
    var value: String?
}

class Valute {
    var charCode: String?
    var value: String?
}
