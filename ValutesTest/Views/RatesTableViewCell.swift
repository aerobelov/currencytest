//
//  RatesTableViewCell.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import UIKit

class RatesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var value: UILabel!
    
    func configure(model: Record) {
        self.date.text = model.date
        self.value.text = model.value
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
