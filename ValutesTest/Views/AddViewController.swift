//
//  AddViewController.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import UIKit

class AddViewController: UIViewController {
    
    @IBOutlet weak var value: UITextField!
    @IBOutlet weak var save: UIButton!
    var model = AddAlertViewModel()
    var onFinish: (()->Void)?
    
    init() {
        super.init(nibName: "AddViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func saveValue() {
        if let doubleValue = self.value.text?.doubleValue() {
            self.model.saveAlert(for: doubleValue)
            self.onFinish?()
        }
    }
    
    func configure() {
        self.value.text = model.getAlert()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configure()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        // Do any additional setup after loading the view.
    }

}
