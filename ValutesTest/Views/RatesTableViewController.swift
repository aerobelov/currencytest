//
//  RatesTableViewController.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import UIKit

class RatesTableViewController: UITableViewController {
    
    var viewModel = RatesViewModel()
    var onAdd: (()->Void)? 
    
    init() {
        super.init(style: .plain)
        let cell = UINib(nibName: "RatesTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "ratesCell")
        
    }
    
    func configure() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add alarm", style: .plain , target: self, action: #selector(addFlow))
        self.title = "USD"
    }
    
    @objc func addFlow() {
        
        print(self.onAdd)
        self.onAdd?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        viewModel.onUpdate = {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        viewModel.fetch()
        
    }
    
   
    
    func viewWillAppear() {
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ratesCell", for: indexPath) as! RatesTableViewCell
        if  viewModel.items.count != 0 {
            cell.configure(model: viewModel.items.reversed()[indexPath.row])
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    

}
