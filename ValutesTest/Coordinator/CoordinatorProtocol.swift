//
//  CoordinatorProtocol.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation

protocol CoordinatorProtocol {
    func start()
}
