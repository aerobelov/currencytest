//
//  Coordinator.swift
//  ValutesTest
//
//  Created by Pavel Belov on 04.11.2021.
//

import Foundation
import UIKit

class Coordinator: CoordinatorProtocol {
    
    var window: UIWindow?
    var ratesListViewController: RatesTableViewController?
    var navController: UINavigationController?
    var addController: AddViewController?
    
    init(window: UIWindow) {
        self.window = window
        self.ratesListViewController = RatesTableViewController()
        self.addController = AddViewController()
        self.navController = UINavigationController(rootViewController: ratesListViewController!)
    }
    
    func addFlow() {
        self.addController?.onFinish = {
            self.listFlow()
        }
        self.navController?.pushViewController(addController!, animated: true)
    }
    
    func listFlow() {
        self.navController?.popViewController(animated: true)
    }
    
   
    
    func start() {
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController = self.navController
        self.ratesListViewController?.onAdd = {
            self.addFlow()
        }
    }
}
